const express = require("express");
const multer = require("multer");
const jwt = require("jsonwebtoken");
const app = express();
const Users = require("../models/Users");
const router = express.Router();
var ip = require("ip");
const Bet = require("../models/Bet");
require("dotenv").config();

const DIR = "uploads/user/profile/";
let storage = multer.diskStorage({
  destination: DIR,
  filename: (req, file, cb) => {
    var nn = file.originalname.replace(".nomedia", "");
    cb(null, Date.now() + "-" + nn);
  },
});
let upload = multer({
  storage: storage,
});
var Address = "http://" + ip.address() + ":" + process.env.PORT;

function verifyJWTToken(token) {
  return new Promise((resolve, reject) => {
    jwt.verify(token, process.env.SECRET_JWT_KEY, (err, tkn) => {
      if (err || !tkn) {
        return reject(err);
      }
      resolve(tkn);
    });
  });
}

function authenticateToken(req, res, next) {
  const token = req.headers.token;

  if (token == null) return res.sendStatus(401);

  jwt.verify(token, process.env.SECRET_JWT_KEY, (err, user) => {
    if (err)
      return res.status(403).json({
        message: "something_went_wrong",
        success: false,
        data: {
          error: err,
        },
      });
    req.user = user;

    next();
  });
}

router.post("/Login", upload.single("profile_picture"), (req, res, next) => {
  console.log();
  var UserId = req.body.UserId;
  var Image = req.body.profile_picture;

  var Message = "";

  Users.findOne({ UserId: UserId }).then((User) => {
    if (User) {
      User.Image = Image;

      Message = "User Login successfully";
    } else {
      User = new Users({
        UserId: UserId,
        Image: Image,
      });
      Message = "User Registration successfully";
    }
    User.save().then(
      (resp) => {
        const JWTToken = jwt.sign({ user: resp }, process.env.SECRET_JWT_KEY, {
          expiresIn: "365d",
        });
        
        return res.status(200).json({
          token: JWTToken,
          message: Message,
          success: true,
          data: User,
        });
      },
      (err) => {
        return res.status(200).json({
          message: "something_went_wrong",
          success: false,
          data: {
            error: err,
          },
        });
      }
    );
  });
});

router.post("/UpdateCoin", authenticateToken, (req, res, next) => {
  var Coin = req.body.Coin;
  Users.findById(req.user.user._id).then((user) => {
    user.Coin = Coin;
    user.save().then((respo) => {
      return res.status(200).json({
        message: "Done",
        success: true,
        data: {
          user: user,
        },
      });
    });
  }),
    (err) => {
      return res.status(401).json({
        message: "session_expired",
        success: false,
        data: {
          error: err,
        },
      });
    };
});

router.get("/GetTopUsers", authenticateToken, (req, res, next) => {
  Users.find({})
    .limit(5)
    .sort({ Coin: -1 })
    .then((UsersList) => {
      return res.status(200).json({
        message: "Done",
        success: true,
        data: { UsersList },
      });
    });
});

router.post("/InsertBet", authenticateToken, (req, res, next) => {
  var BetName = req.body.BetName;
  var BetType = req.body.BetType;
  var BetAmount = req.body.BetAmount;
  BetObj = new Bet({
    UserId: req.user.user._id,
    BetName: BetName,
    BetType: BetType,
    BetAmount: BetAmount,
  });
  BetObj.save().then((respo) => {
    return res.status(200).json({
      message: "Done",
      success: true,
      data: BetObj,
    });
  }),
    (err) => {
      return res.status(401).json({
        message: "something_went_wrong",
        success: false,
        data: {
          error: err,
        },
      });
    };
});

router.get("/GetBets", authenticateToken, (req, res, next) => {
  console.log(req.user.user._id);
  Bet.find({ UserId: req.user.user._id })
    .limit(3)
    .sort({ created_at: -1 })
    .then((BetList) => {
      return res.status(200).json({
        message: "Done",
        success: true,
        data: { BetList },
      });
    });
});

module.exports = router;
