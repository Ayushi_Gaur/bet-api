const mongoose = require("mongoose");
const express = require("express");
const user_route = require('./routes/user_route')
require("dotenv").config();

mongoose.connect("mongodb://BetUser:Bet123456@localhost:27017/bet", {
   useNewUrlParser: true,
   useUnifiedTopology: true,
   useFindAndModify: false
});
const app = express();
const connection = mongoose.connection;
connection.once("open", () => {
  console.log("MongoDB database connection established successfully!");
});
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb', extended: true }))


const server = app.listen(process.env.PORT, process.env.BASE_URL);
// const io = socketIo().listen(server);
console.log(
  "database",
  process.env.DATABASE_URL,
  "app running on port ",
  process.env.PORT
);

app.use("/user", user_route);
app.use("/uploads", express.static("uploads"));


