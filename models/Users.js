const mongoose =require('mongoose') 
const Schema = mongoose.Schema;
var autoIncrement = require('mongoose-auto-increment');
mongoose.set('useCreateIndex', true);


let Users = new Schema({
    UserId: {
        type: String,
        default: null
    },
    Image: {
        type: String,
        default: null
    },
    Coin:{
        type:Number,
        default:null
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    updated_at: {
        type: Date,
        default: Date.now
    },
});
autoIncrement.initialize(mongoose.connection);
Users.plugin(autoIncrement.plugin, {
    model: 'UsersCounter',
    field: '_id',
    startAt: 1,
    incrementBy: 1
});
module.exports = mongoose.model('Users', Users);
