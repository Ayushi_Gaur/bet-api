const mongoose =require('mongoose') 
const Schema = mongoose.Schema;
var autoIncrement = require('mongoose-auto-increment');
mongoose.set('useCreateIndex', true);


let Bet = new Schema({
    UserId: {
        type: String,
        default: null,
    },
    BetName: {
        type: String,
        default: null
    },
    BetType: {
        type: Number,
        default: null
    },
    BetAmount:{
        type:Number,
        default:null
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    updated_at: {
        type: Date,
        default: Date.now
    },
});
autoIncrement.initialize(mongoose.connection);
Bet.plugin(autoIncrement.plugin, {
    model: 'BetCounter',
    field: '_id',
    startAt: 1,
    incrementBy: 1
});
module.exports = mongoose.model('Bet', Bet);
